## API End points

# BASE_URL should be the path to project root (Like http://localhost/advertisement/) 
# But most of the time Codeigniter will handle this

# Get the adv which match the {keyword}
# api/adv/keyword/{keyword} 
# API URL - BASE_URL/api/adv/keyword/{keyword}

# Update the click count of the adv which match the {keyword}
# api/addClick/keyword/{keyword} 
# API URL - BASE_URL/api/addClick/keyword/{keyword} 

# Update the visit count of the adv which match the {keyword}
# api/addVisit/keyword/{keyword} 
# API URL - BASE_URL/api/addVisit/keyword/{keyword}

# For Example - On demo server

# api/adv/keyword/{keyword} 
	http://codebases.com/ads/api/adv/keyword/demo

# api/addClick/keyword/{keyword} 	
	http://codebases.com/ads/api/addClick/keyword/demo
	
# api/addVisit/keyword/{keyword} 
 http://codebases.com/ads/api/addVisit/keyword/demo