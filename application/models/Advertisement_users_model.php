<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Advertisement_Users_Model extends MY_Model {

    public $_table = 'advertisement_users';
    protected $primary_key = 'id';

    public function __construct() {
        parent::__construct();
    }

    // Get the users assigned to a adv
    public function get_adv_users($conditions = array()) {
        $this->db->select(array('users.*'));
        $this->db->from('users');

        $this->db->join('advertisement_users', 'advertisement_users.user_id = users.user_id');

        if (!empty($conditions)) {
            $this->db->where($conditions);
        }

        return $this->db->get()->result_array();
    }

    // Get the advs assigned to a user
    public function get_user_advs($conditions = array()) {
        $this->db->select(array('advertisement.*'));
        $this->db->from('advertisement_users');

        $this->db->join('advertisement', 'advertisement.advertisement_id = advertisement_users.advertisement_id');

        if (!empty($conditions)) {
            $this->db->where($conditions);
        }

        return $this->db->get()->result_array();
    }

}
