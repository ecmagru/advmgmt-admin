<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Advertisement_Keyword_Model extends MY_Model {

    public $_table = 'advertisement_keyword';
    protected $primary_key = 'id';

    public function __construct() {
        parent::__construct();
    }

    // Get all the ads with keywords
    public function get_all_advertisement($conditions = array()) {
        $this->db->select(array('advertisement.*', 'advertisement_keyword.keywords', 'users.name'));
        $this->db->from('advertisement');

        if ($date_from) {
            $this->db->where('date(advertisement.date_created) >=', $date_from);
        }

        if ($date_to) {
            $this->db->where('date(advertisement.date_created) <=', $date_to);
        }

        if (!empty($conditions)) {
            $this->db->where($conditions);
        }

        $this->db->join('advertisement_keyword', 'advertisement_keyword.advertisement_id = advertisement.advertisement_id');
        $this->db->join('users', 'users.user_id = advertisement_keyword.user_id', 'LEFT');

        return $this->db->get()->result_array();
    }

}
