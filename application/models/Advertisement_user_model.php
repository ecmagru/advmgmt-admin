<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Advertisement_User_Model extends MY_Model {

    public $_table = 'advertisement_user';
    protected $primary_key = 'id';

    public function __construct() {
        parent::__construct();
    }

}
