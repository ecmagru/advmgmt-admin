<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users_Model extends MY_Model {

    public $_table = 'users';
    protected $primary_key = 'user_id';

    public function __construct() {
        parent::__construct();
    }

}