<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Log_Model extends MY_Model {

    public $_table = 'log';
    protected $primary_key = 'log_id';
    protected $soft_delete = TRUE;

    public function __construct() {
        parent::__construct();
    }

    // Admin - Get all the logs
    public function get_logs($date_from, $date_to, $advertisement_id) {
        $this->db->select(array('log.*', 'advertisement.title'));
        $this->db->from('log');

        if ($date_from) {
            $this->db->where('date(log.date_created) >=', $date_from);
        }

        if ($date_to) {
            $this->db->where('date(log.date_created) <=', $date_to);
        }
        
        $this->db->where(array('advertisement.unique_id' =>  $advertisement_id));

        $this->db->join('advertisement', 'log.advertisement_id = advertisement.advertisement_id');
        return $this->db->get()->result_array();
    }

}
