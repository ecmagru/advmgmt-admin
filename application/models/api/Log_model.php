<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Log_Model extends MY_Model {

    public $_table = 'log';
    protected $primary_key = 'log_id';
    protected $soft_delete = TRUE;

    public function __construct() {
        parent::__construct();
    }

}
