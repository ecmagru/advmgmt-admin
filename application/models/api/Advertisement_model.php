<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Advertisement_Model extends MY_Model {

    public $_table = 'advertisement';
    protected $primary_key = 'advertisement_id';
    protected $soft_delete = TRUE;

    public function __construct() {
        parent::__construct();
    }

    public function get_advertisement($keyword = NULL) {
        $this->db->select(array('advertisement.*', 'advertisement_keyword.keyword'));
        $this->db->from('advertisement');

        if (!is_null($keyword)) {
            $this->db->like('advertisement_keyword.keyword', $keyword);
        }

        $this->db->join('advertisement_keyword', 'advertisement_keyword.advertisement_id = advertisement.advertisement_id');

        return $this->db->get()->result_array();
    }

}
