<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Advertisement_Model extends MY_Model {

    public $_table = 'advertisement';
    protected $primary_key = 'advertisement_id';

    public function __construct() {
        parent::__construct();
    }

    // Get all the ads
    public function get_all_advertisement($date_from, $date_to, $conditions = array()) {
        $this->db->select(array('advertisement.*', 'advertisement_keyword.keyword', 'users.name'));
        $this->db->from('advertisement');

        if ($date_from) {
            $this->db->where('date(advertisement.date_created) >=', $date_from);
        }

        if ($date_to) {
            $this->db->where('date(advertisement.date_created) <=', $date_to);
        }

        if (!empty($conditions)) {
            $this->db->where($conditions);
        }

        $this->db->join('advertisement_keyword', 'advertisement_keyword.advertisement_id = advertisement.advertisement_id');
        $this->db->join('advertisement_user', 'advertisement_user.advertisement_id = advertisement_keyword.advertisement_id', 'LEFT');
        $this->db->join('users', 'users.user_id = advertisement_user.user_id', 'LEFT');

        return $this->db->get()->result_array();
    }

    public function get_advertisement($conditions = array()) {
        $this->db->select(array('advertisement.*', 'advertisement_keyword.keyword'));
        $this->db->from('advertisement');

        if (!empty($conditions)) {
            $this->db->where($conditions);
        }

        $this->db->join('advertisement_keyword', 'advertisement_keyword.advertisement_id = advertisement.advertisement_id');

        $result = $this->db->get()->result_array();

        // Format the result
        $advertisement = array();
        $advertisement_id = NULL;

        if (!empty($result)) {
            foreach ($result as $row) {
                if ($advertisement_id != $row['advertisement_id']) {
                    $advertisement_id = $row['advertisement_id'];

                    $advertisement['advertisement_id'] = $row['advertisement_id'];
                    $advertisement['title'] = $row['title'];
                    $advertisement['traffic_string'] = $row['traffic_string'];
                    $advertisement['url'] = $row['url'];
                    $advertisement['image_url'] = $row['image_url'];
                    $advertisement['visit_count'] = $row['visit_count'];
                    $advertisement['click_count'] = $row['click_count'];
                    $advertisement['status'] = $row['status'];
                    $advertisement['unique_id'] = $row['unique_id'];
                    $advertisement['keywords'] = array();
                }

                array_push($advertisement['keywords'], $row['keyword']);
            }
        }

        return $advertisement;
    }

    public function get_user_advertisements($conditions) {
        $this->db->select(array('advertisement.*', 'advertisement_keyword.keyword'));
        $this->db->from('advertisement');

        $this->db->where($conditions);
        $this->db->where(array('advertisement.status' => 1));

        $this->db->join('advertisement_keyword', 'advertisement_keyword.advertisement_id = advertisement.advertisement_id');
        $this->db->join('advertisement_user', 'advertisement_user.advertisement_id = advertisement.advertisement_id');

        $advertisement_list = $this->db->get()->result_array();

        // Format the result
        $advertisement_id = NULL;
        $adv_array = array();

        if (!empty($advertisement_list)) {
            foreach ($advertisement_list as $row) {
                if ($advertisement_id != $row['advertisement_id']) {
                    $advertisement_id = $row['advertisement_id'];

                    $adv_array[$advertisement_id]['advertisement_id'] = $row['advertisement_id'];
                    $adv_array[$advertisement_id]['title'] = $row['title'];
                    $adv_array[$advertisement_id]['url'] = $row['url'];
                    $adv_array[$advertisement_id]['status'] = $row['status'];
                    $adv_array[$advertisement_id]['unique_id'] = $row['unique_id'];
                    $adv_array[$advertisement_id]['date_created'] = $row['date_created'];
                    $adv_array[$advertisement_id]['keywords'] = array();
                }

                array_push($adv_array[$advertisement_id]['keywords'], $row['keyword']);
            }
        }

        return $adv_array;
    }

    // Get the adv that are not assigned
    public function get_advertisements_not_assigned() {
        $this->db->select(array('advertisement_user.*', 'advertisement.title', 'advertisement.advertisement_id'));
        $this->db->from('advertisement');

        $this->db->where(array('advertisement.status' => 1));

        $this->db->join('advertisement_user', 'advertisement_user.advertisement_id = advertisement.advertisement_id', 'LEFT');

        return $this->db->get()->result_array();
    }

}
