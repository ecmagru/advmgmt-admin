<style>            
    .bar {
        height: 18px;
        background: green;
    }
</style>

<h4>Create Advertisement</h4>

<?php echo form_open(base_url('user/advertisement/create')); ?>
<div class="form-group <?php echo (form_error('title')) ? 'has-error' : ''; ?>">
    <div class="input-group">
        <input type="text" class="form-control" placeholder="Title" name="title" value="<?php echo set_value('title'); ?>">
        <?php echo (form_error('title')) ? form_error('title') : ''; ?>
    </div>
</div>
<div class="form-group <?php echo (form_error('url')) ? 'has-error' : ''; ?>">
    <div class="input-group">
        <input type="text" class="form-control" placeholder="URL" name="url" value="<?php echo set_value('url'); ?>">
        <?php echo (form_error('url')) ? form_error('url') : ''; ?>
    </div>
</div>
<div class="form-group <?php echo (form_error('keyword')) ? 'has-error' : ''; ?>">
    <div class="input-group">
        <input type="text" class="form-control" placeholder="Keyword" name="keyword" value="<?php echo set_value('keyword'); ?>">
        <?php echo (form_error('keyword')) ? form_error('keyword') : ''; ?>
    </div>
</div>

<div class="form-group">
    <div class="input-group">
        <?php if (isset($_POST['file_name']) && $_POST['file_name']): ?>
            <img src="<?php echo $_POST['file_url']; ?>" id="image_placeholder" />
        <?php else: ?>
            <img src="http://placehold.it/200x170" id="image_placeholder" />
        <?php endif; ?> <br/><br/>
        <input type="file" name="file" id="file_upload" data-url="<?php echo base_url('uploader'); ?>" data-form-data='{"folder": "uploads"}' />
        <?php echo (form_error('file_name')) ? form_error('file_name') : ''; ?>
        <input type="hidden" name="file_name" id="file_name" value="<?php echo isset($_POST['file_name']) ? $_POST['file_name'] : ''; ?>" />
        <input type="hidden" name="file_url" id="file_url" value="<?php echo isset($_POST['file_url']) ? $_POST['file_url'] : ''; ?>" />
    </div>
</div>

<div id="progress">
    <div class="bar" style="width: 0%;"></div>
</div>

<button type="submit" class="btn btn-primary" name="submit" value="submit">Submit</button>
<?php echo form_close(); ?>
