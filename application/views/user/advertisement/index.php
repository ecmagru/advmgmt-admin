<!-- <?php echo anchor(base_url('user/advertisement/create'), 'Create New', array('class' => 'btn btn-primary btn-sm')); ?> -->
<br/><br/>

<?php echo $flashdata; ?>

<?php if (empty($advertisement_list)): ?>

    <div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Hey!</strong> Nothing found
    </div>

<?php else: ?>

    <table class="table table-bordered table-hover table-responsive">
        <thead>
            <tr>
                <th width="30%">Title</th>
                <th width="10%">Keywords</th>
                <th width="20%">URL</th>
                <th width="15%">Date</th>
                <th width="10%">Status</th>
                <th width="15%">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($advertisement_list as $row): ?>
                <tr>
                    <td><?php echo $row['title']; ?></td>
                    <td><?php echo implode(', ', $row['keywords']); ?></td>
                    <td><?php echo $row['url']; ?></td>
                    <td><?php echo $row['date_created']; ?></td>
                    <td>
                        <?php if ($row['status'] == 1): ?>
                            <span class="label label-primary">Active</span>
                        <?php else: ?>
                            <span class="label label-danger">Inactive</span>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php echo anchor(base_url('user/advertisement/log/' . $row['unique_id']), 'Log'); ?>
                     <!--   <?php echo anchor(base_url('user/advertisement/edit/' . $row['unique_id']), 'Edit'); ?> --> &nbsp;&nbsp;
                      <!--   <?php echo anchor(base_url('user/advertisement/delete/' . $row['unique_id']), 'Delete', array('class' => 'row-delete')); ?> -->
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

<?php endif; ?>