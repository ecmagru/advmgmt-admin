<br/><br/>

<?php echo $flashdata; ?>

<?php echo form_open(base_url('user/advertisement/log/' . $advertisement_id), array('class' => 'form-inline', 'method' => 'get')); ?>
<div class="form-group">
    <input type="text" class="form-control datepicker" placeholder="Date From" name="date_from" value="<?php echo $date_from; ?>">
</div>
<div class="form-group">
    <input type="text" class="form-control datepicker" placeholder="Date To" name="date_to" value="<?php echo $date_to; ?>">
</div>
<button type="submit" class="btn btn-primary" name="submit" value="SEARCH">Search</button>
<?php echo form_close(); ?>
<br/>

<?php if (empty($logs)): ?>

    <div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Hey!</strong> Nothing found
    </div>

<?php else: ?>

    <table class="table table-bordered table-hover table-responsive table-striped">
        <thead>
            <tr>
                <th width="20%">Keyword</th>
                <th width="15%">Date</th>
            </tr>
        </thead>
        <?php foreach ($logs as $row): ?>
            <tr>
                <td><?php echo $row['keyword']; ?></td>
                <td><?php echo $row['date_created']; ?></td>
            </tr>
        <?php endforeach; ?>
    </table>

<?php endif; ?>
