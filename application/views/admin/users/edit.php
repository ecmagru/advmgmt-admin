<h4>Edit User</h4>

<?php echo form_open(base_url('admin/users/edit/' . $user['user_id'])); ?>
<div class="form-group <?php echo (form_error('name')) ? 'has-error' : ''; ?>">
    <div class="input-group">
        <input type="text" class="form-control" placeholder="Name" name="name" value="<?php echo set_value('name', $user['name']); ?>">
        <?php echo form_error('name'); ?>
    </div>
</div>

<div class="form-group <?php echo (form_error('email')) ? 'has-error' : ''; ?>">
    <div class="input-group">
        <input type="text" class="form-control" placeholder="Email" name="email" value="<?php echo set_value('email', $user['email']); ?>">
        <?php echo form_error('email'); ?>
    </div>
</div>

<div class="form-group <?php echo (form_error('user_role')) ? 'has-error' : ''; ?>">
    <label>User Role</label>
    <div class="input-group">
        <select name="user_role" class="form-control">
            <option value="1" <?php echo set_select('user_role', 1, (($user['type'] == 1) ? TRUE : FALSE)); ?>>User</option>
            <option value="2" <?php echo set_select('user_role', 2, (($user['type'] == 2) ? TRUE : FALSE)); ?>>Administrator</option>
        </select>
        <?php echo form_error('user_role'); ?>
    </div>
</div>

<div class="form-group <?php echo (form_error('advertisements')) ? 'has-error' : ''; ?>">
    <label>Select the Adv (Press Ctrl to select multiple)</label>
    <div class="input-group">
        <select name="advertisements[]" multiple="multiple" style="width:300px; height: 300px;">
            <?php if (!empty($advertisements)): ?>
                <?php foreach ($advertisements as $row): ?>
                    <?php if (!$row['user_id'] || $row['user_id'] == $user['user_id']): ?>
                        <option value="<?php echo $row['advertisement_id']; ?>" <?php echo ($row['user_id'] == $user['user_id']) ? 'selected' : ''; ?> ><?php echo $row['title']; ?></option>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
        </select>
        <?php echo form_error('advertisements'); ?>
    </div>
</div>

<button type="submit" class="btn btn-primary" name="submit" value="submit">Submit</button>
<?php echo form_close(); ?>