<h4>Create User</h4>

<?php echo form_open(base_url('admin/users/create')); ?>
<div class="form-group <?php echo (form_error('name')) ? 'has-error' : ''; ?>">
    <div class="input-group">
        <input type="text" class="form-control" placeholder="Name" name="name" value="<?php echo set_value('name'); ?>">
        <?php echo form_error('name'); ?>
    </div>
</div>

<div class="form-group <?php echo (form_error('email')) ? 'has-error' : ''; ?>">
    <div class="input-group">
        <input type="text" class="form-control" placeholder="Email" name="email" value="<?php echo set_value('email'); ?>">
        <?php echo form_error('email'); ?>
    </div>
</div>

<div class="form-group <?php echo (form_error('password')) ? 'has-error' : ''; ?>">
    <div class="input-group">
        <input type="password" class="form-control" placeholder="Password" name="password" value="<?php echo set_value('password'); ?>">
        <?php echo form_error('password'); ?>
    </div>
</div>

<div class="form-group <?php echo (form_error('user_role')) ? 'has-error' : ''; ?>">
    <label>User Role</label>
    <div class="input-group">
        <select name="user_role" class="form-control">
            <option value="1">User</option>
            <option value="2">Administrator</option>
        </select>
        <?php echo form_error('user_role'); ?>
    </div>
</div>

<div class="form-group <?php echo (form_error('advertisements')) ? 'has-error' : ''; ?>">
    <label>Select the Adv (Press Ctrl to select multiple)</label>
    <div class="input-group">
        <select name="advertisements[]" multiple="multiple" style="width:300px; height: 300px;">
            <?php if (!empty($advertisements)): ?>
                <?php foreach ($advertisements as $row): ?>
                    <?php if (!$row['user_id']): ?>
                        <option value="<?php echo $row['advertisement_id']; ?>"><?php echo $row['title']; ?></option>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
        </select>
        <?php echo form_error('advertisements'); ?>
    </div>
</div>

<button type="submit" class="btn btn-primary" name="submit" value="submit">Submit</button>
<?php echo form_close(); ?>