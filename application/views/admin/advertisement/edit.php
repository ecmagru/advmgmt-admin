<style>            
    .bar {
        height: 18px;
        background: green;
    }
</style>

<h4>Edit Advertisement</h4>

<div class="form-group">
    <label>Visit Count</label>
    <div class="input-group">
        <input type="text" class="form-control" value="<?php echo $advertisement['visit_count']; ?>" disabled="disabled">
    </div>
</div>
<div class="form-group">
    <label>Click Count</label>
    <div class="input-group">
        <input type="text" class="form-control" value="<?php echo $advertisement['click_count']; ?>" disabled="disabled">
    </div>
</div>

<?php echo form_open(base_url('admin/advertisement/edit/' . $advertisement['unique_id'])); ?>
<div class="form-group <?php echo (form_error('title')) ? 'has-error' : ''; ?>">
    <div class="input-group">
        <label>Title</label>
        <input type="text" class="form-control" placeholder="Title" name="title" value="<?php echo set_value('title', $advertisement['title']); ?>">
        <?php echo (form_error('title')) ? form_error('title') : ''; ?>
    </div>
</div>
<div class="form-group <?php echo (form_error('url')) ? 'has-error' : ''; ?>">
    <div class="input-group">
        <label>URL</label>
        <input type="text" class="form-control" placeholder="URL" name="url" value="<?php echo set_value('url', $advertisement['url']); ?>">
        <?php echo (form_error('url')) ? form_error('url') : ''; ?>
    </div>
</div>
<div class="form-group <?php echo (form_error('keyword')) ? 'has-error' : ''; ?>">
    <div class="input-group">
        <label>Keyword</label>
        <input type="text" class="form-control" placeholder="Keyword" name="keyword" value="<?php echo set_value('keyword', implode(', ', $advertisement['keywords'])); ?>">
        <?php echo (form_error('keyword')) ? form_error('keyword') : ''; ?>
    </div>
</div>

<div class="form-group">
    <div class="input-group">
        <?php if ($file_name): ?>
            <img src="<?php echo $file_url; ?>" id="image_placeholder" />
        <?php else: ?>
            <img src="http://placehold.it/200x170" id="image_placeholder" />
        <?php endif; ?> <br/><br/>

        <input type="file" name="file" id="file_upload" data-url="<?php echo base_url('uploader'); ?>" data-form-data='{"folder": "uploads"}' />
        <?php echo (form_error('file_name')) ? form_error('file_name') : ''; ?>
        <input type="hidden" name="file_name" id="file_name" value="<?php echo set_value('file_name', $file_name); ?>" />
        <input type="hidden" name="file_url" id="file_url" value="<?php echo set_value('file_url', $file_url); ?>" />
    </div>
</div>

<div id="progress">
    <div class="bar" style="width: 0%;"></div>
</div>
<input type="hidden" name="advertisement_id" value="<?php echo $advertisement['advertisement_id']; ?>" />
<button type="submit" class="btn btn-primary" name="submit" value="submit">Submit</button>
<?php echo form_close(); ?>
