<?php echo anchor(base_url('admin/advertisement/create'), 'Create New', array('class' => 'btn btn-primary btn-sm')); ?>

<br/><br/>

<?php echo $flashdata; ?>

<?php echo form_open(base_url('admin/advertisement/index'), array('class' => 'form-inline', 'method' => 'get')); ?>
<div class="form-group">
    <input type="text" class="form-control datepicker" placeholder="Date From" name="date_from" value="<?php echo $date_from; ?>">
</div>
<div class="form-group">
    <input type="text" class="form-control datepicker" placeholder="Date To" name="date_to" value="<?php echo $date_to; ?>">
</div>
<button type="submit" class="btn btn-primary" name="submit" value="SEARCH">Search</button>
<?php echo form_close(); ?>
<br/>

<?php if (empty($advertisement_list)): ?>

    <div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Hey!</strong> Nothing found
    </div>

<?php else: ?>

    <table class="table table-bordered table-hover table-responsive">
        <thead>
            <tr>
                <th width="20%">Title</th>
                <th width="20%">User Assigned</th>
                <th width="10%">Keywords</th>
                <th width="20%">URL</th>
                <th width="5%">Click Count</th>
                <th width="5%">Visit Count</th>
                <th width="25%">Date</th>
                <th width="10%">Status</th>
                <th width="30%">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($advertisement_list as $row): ?>
                <tr>
                    <td><?php echo $row['title']; ?></td>
                    <td><?php echo $row['user_assigned']; ?></td>
                    <td><?php echo implode(', ', $row['keywords']); ?></td>
                    <td><?php echo $row['url']; ?></td>
                    <td><?php echo $row['click_count']; ?></td>
                    <td><?php echo $row['visit_count']; ?></td>
                    <td><?php echo $row['date_created']; ?></td>
                    <td>
                        <?php if ($row['status'] == 1): ?>
                            <span class="label label-primary">Active</span>
                        <?php else: ?>
                            <span class="label label-danger">Inactive</span>
                        <?php endif; ?>
                    </td>
                    <td>
                        <!--    <?php echo anchor(base_url('admin/advertisement/users/' . $row['unique_id']), 'Users'); ?>  -->&nbsp;
                        <?php echo anchor(base_url('admin/advertisement/log/' . $row['unique_id']), 'Log'); ?>
                        <?php echo anchor(base_url('admin/advertisement/edit/' . $row['unique_id']), 'Edit'); ?>
                        <?php echo anchor(base_url('admin/advertisement/delete/' . $row['unique_id']), 'Delete', array('class' => 'row-delete')); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

<?php endif; ?>