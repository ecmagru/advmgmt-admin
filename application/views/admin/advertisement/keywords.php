<br/><br/>

<?php if (empty($keywords)): ?>

    <div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Hey!</strong> Nothing found
    </div>

<?php else: ?>

    <table class="table table-bordered table-hover table-responsive table-striped">
        <thead>
            <tr>
                <th width="30%">Keywords</th>
                <th width="50%">Adv Title</th>
                <th width="20%">User Assigned</th>
            </tr>
        </thead>
        <?php foreach ($keywords as $row): ?>
            <tr>
                <td><?php echo $row['keyword']; ?></td>
                <td><?php echo $row['title']; ?></td>
                <td><?php echo $row['name']; ?></td> 
            </tr>
        <?php endforeach; ?>
    </table>

<?php endif; ?>