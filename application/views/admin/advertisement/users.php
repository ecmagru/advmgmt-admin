<br/><br/>

<?php if (empty($users)): ?>

    <div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Hey!</strong> Nothing found
    </div>

<?php else: ?>

    <table class="table table-bordered table-hover table-responsive table-striped">
        <thead>
            <tr>
                <th width="30%">Name</th>
                <th width="30%">Email</th>
                <th width="10%">Role</th>
                <th width="10%">Status</th>
                <th width="10%">Date</th>
                <th width="10%">Action</th>
            </tr>
        </thead>
        <?php foreach ($users as $row): ?>
            <tr>
                <td><?php echo $row['name']; ?></td>
                <td><?php echo $row['email']; ?></td>
                <td>
                    <?php if ($row['type'] == 1): ?>
                        <span class="label label-primary">User</span>
                    <?php elseif ($row['type'] == 2): ?>
                        <span class="label label-danger">Administrator</span>
                    <?php endif; ?>
                </td>
                <td>
                    <?php if ($row['status'] == 1): ?>
                        <span class="label label-primary">Active</span>
                    <?php elseif ($row['status'] == 0): ?>
                        <span class="label label-danger">Inactive</span>
                    <?php endif; ?>
                </td>
                <td>
                    <?php echo $row['date_created']; ?>
                </td>
                <td>
                    <?php echo anchor(base_url('admin/users/edit/' . $row['user_id']), 'Edit'); ?> &nbsp;
                    <?php echo anchor(base_url('admin/users/delete/' . $row['user_id']), 'Delete', array('class' => 'row-delete')); ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>

<?php endif; ?>