<?php $flashdata = $this->session->flashdata('flashdata'); ?>

<?php if (is_array($flashdata) && isset($flashdata['type'], $flashdata['text'])): ?>

    <?php if ($flashdata['type'] == 'error'): ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Error!</strong> <?php echo $flashdata['text']; ?>
        </div>
    <?php endif; ?>

    <?php if ($flashdata['type'] == 'success'): ?>
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Success!</strong> <?php echo $flashdata['text']; ?>
        </div>
    <?php endif; ?>

<?php endif; ?>

