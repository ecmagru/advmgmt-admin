<h4>Sign Up</h4>
<?php echo form_open(base_url('signup')); ?>
<div class="form-group <?php echo (form_error('name')) ? 'has-error' : ''; ?>">
    <div class="input-group">
        <input type="text" class="form-control" placeholder="Name" name="name" value="<?php echo set_value('name'); ?>">
        <?php echo (form_error('name')) ? form_error('name') : ''; ?>
    </div>
</div>
<div class="form-group <?php echo (form_error('email')) ? 'has-error' : ''; ?>">
    <div class="input-group">
        <input type="email" class="form-control" placeholder="Email" name="email" value="<?php echo set_value('email'); ?>">
        <?php echo (form_error('email')) ? form_error('email') : ''; ?>
    </div>
</div>
<div class="form-group <?php echo (form_error('password')) ? 'has-error' : ''; ?>">
    <div class="input-group">
        <input type="password" class="form-control" placeholder="Password" name="password" value="">
        <?php echo (form_error('password')) ? form_error('password') : ''; ?>
    </div>
</div>
<button type="submit" class="btn btn-primary" name="submit" value="submit">Submit</button>
<?php echo form_close(); ?>
