<?php echo $flashdata; ?>

<h4>Sign In</h4>
<?php echo form_open(base_url('signin')); ?>
<div class="form-group <?php echo (form_error('name')) ? 'has-error' : ''; ?>">
    <div class="input-group">
        <input type="email" class="form-control" placeholder="Email" name="email">
        <?php echo (form_error('email')) ? form_error('email') : ''; ?>
    </div>
</div>
<div class="form-group">
    <div class="input-group <?php echo (form_error('password')) ? 'has-error' : ''; ?>">
        <input type="password" class="form-control" placeholder="Password" name="password">
        <?php echo (form_error('password')) ? form_error('password') : ''; ?>
    </div>
</div>
<button type="submit" class="btn btn-primary" name="submit" value="submit">Submit</button>
<?php echo form_close(); ?>
