<?php

// Messages
$config['error_message'] = 'Unexpected error has been occured. Please try again';
$config['error_invalid_login'] = 'Invalid username or password';

$config['success_signup'] = 'Successfully created account';
$config['success_advertisement_created'] = 'Successfully created advertisement';
$config['success_advertisement_deleted'] = 'Successfully deleted advertisement';
$config['success_advertisement_updated'] = 'Successfully updated advertisement';

$config['success_user_deleted'] = 'Successfully deleted user';
$config['error_deny_user_deletion'] = 'Super admin deletion not possible';

$config['success_user_created'] = 'Successfully created user';
$config['success_user_updated'] = 'Successfully updated user';
