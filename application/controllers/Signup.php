<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Signup extends Frontend_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('users_model');
    }

    public function index() {
        $this->load->library('form_validation');

        if ($this->input->post('submit') && $this->input->post('submit') == 'submit') {
            $this->form_validation->set_rules('name', 'Name', 'required|trim|max_length[100]');
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|max_length[100]');
            $this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[6]|max_length[30]');

            $this->form_validation->set_message('required', '%s is required');
            $this->form_validation->set_error_delimiters('<p class="help-block">', '</p>');

            if ($this->form_validation->run() == TRUE) {
                $formData = array(
                    'name' => $this->input->post('name'),
                    'email' => $this->input->post('email'),
                    'password' => md5($this->input->post('password')),
                    'date_created' => date('Y-m-d H:i:s')
                );

                if ($this->users_model->insert($formData)) {
                    $this->session->set_flashdata('flashdata', array('type' => 'success', 'text' => config_item('success_signup')));
                    redirect(base_url('signin'));
                    exit();
                }
            }
        }

        $this->template->build('frontend/signup', $this->data);
    }

}
