<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Uploader extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $userdata = $this->session->userdata('user_data');
        if (empty($userdata)) {
            die(json_encode(array('error' => 'Action not allowed')));
        }

        $this->load->helper(array('form', 'url', 'file'));
    }

    public function index() {
        $upload_path_url = base_url() . $_REQUEST['folder'] . '/';

        $config['upload_path'] = FCPATH . $_REQUEST['folder'] . '/';
        $config['allowed_types'] = 'jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF';
        $config['max_size'] = '30000';
        $config['encrypt_name'] = TRUE;
        $config['file_name'] = uniqid() . md5(time());

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('file')) {
            $data = $this->upload->data();

            /* $config = array();
              $config['image_library'] = 'gd2';
              $config['source_image'] = $data['full_path'];
              $config['create_thumb'] = TRUE;
              $config['new_image'] = $data['file_path'] . 'thumbs/';
              $config['maintain_ratio'] = TRUE;
              $config['thumb_marker'] = '';

              $this->load->library('image_lib', $config);
              $this->image_lib->resize(); */

            //set the data for the json array
            $info = new StdClass;
            $info->name = $data['file_name'];
            $info->size = $data['file_size'] * 1024;
            $info->type = $data['file_type'];
            $info->url = $upload_path_url . $data['file_name'];
            $info->error = null;

            if ($this->input->is_ajax_request()) {
                die(json_encode(array("files" => $info)));
            }
        }

        // On error
        die(json_encode(array('error' => $this->upload->display_errors())));
    }

}
