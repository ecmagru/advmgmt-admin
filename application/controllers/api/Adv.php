<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Adv extends REST_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('api/advertisement_model');
    }

    /*  public function index_get() {
      $result = $this->advertisement_model->get_advertisement('cc');
      $adv_index = $this->filter_adv($result, 'cc');
      print_r($result[$adv_index]);
      }
     */

    // Get the adv with keywords
    public function advertisements_get($keyword = NULL) {
        $keyword = strtolower(urldecode($keyword));

        if (is_null($keyword)) {
            $this->response(NULL, 200);
        }

        $result = $this->advertisement_model->get_advertisement($keyword);
        // $result = $this->advertisement_model->get_all_advertisement(NULL, NULL, $keyword);

        if (empty($result)) {
            $this->response(array('error' => 'No Advertisement Found'), 200);
        } else {
            $result = $result[$this->filter_adv($result, $keyword)];
            // echo $result['advertisement_id']; exit;
            $this->update_log($result['advertisement_id'], $keyword, "visit");
            $result['image_url'] = base_url('uploads/' . $result['image_url']);
            $visit_count = $result['visit_count'] + 1;
            $this->advertisement_model->update_by(array('advertisement_id' => $result['advertisement_id']), array('visit_count' => $visit_count));
            $this->response($result, 200);
        }
    }

    // Click count
    public function addClick_get($keyword = NULL) {
        $keyword = strtolower(urldecode($keyword));

        if (is_null($keyword)) {
            $this->response(NULL, 200);
        }

        $result = $this->advertisement_model->get_advertisement($keyword);

        if (empty($result)) {
            $this->response(array('error' => 'No Advertisement Found'), 200);
        } else {
            $result = $result[$this->filter_adv($result, $keyword)];
            $this->update_log($result['advertisement_id'], $keyword, "click");

            // Update the click count
            $click_count = $result['click_count'] + 1;
            $this->advertisement_model->update_by(array('advertisement_id' => $result['advertisement_id']), array('click_count' => $click_count));
            $this->response(array('success' => 'Click count updated', 'click_count' => $click_count), 200);
        }
    }

    // Click count
    public function click_get($keyword = NULL) {
        $keyword = strtolower(urldecode($keyword));
        $url = $_GET['redir'];

        if (is_null($keyword)) {
            $this->response(NULL, 200);
        }

        $result = $this->advertisement_model->get_advertisement($keyword);

        if (empty($result)) {
            $this->response(array('error' => 'No Advertisement Found'), 200);
        } else {
            $result = $result[$this->filter_adv($result, $keyword)];
            $this->update_log($result['advertisement_id'], $keyword, "click");

            // Update the click count
            $click_count = $result['click_count'] + 1;
            $this->advertisement_model->update_by(array('advertisement_id' => $result['advertisement_id']), array('click_count' => $click_count));
            redirect($url);
        }
    }

    // Visit count
    public function addVisit_get($keyword = NULL) {
        $keyword = strtolower(urldecode($keyword));

        if (is_null($keyword)) {
            $this->response(NULL, 200);
        }

        $result = $result = $this->advertisement_model->get_advertisement($keyword);

        if (empty($result)) {
            $this->response(array('error' => 'No Advertisement Found'), 200);
        } else {
            $result = $result[$this->filter_adv($result, $keyword)];
            $this->update_log($result['advertisement_id'], $keyword, "visit");

            // Update the click count
            $visit_count = $result['visit_count'] + 1;
            $this->advertisement_model->update_by(array('advertisement_id' => $result['advertisement_id']), array('visit_count' => $visit_count));
            $this->response(array('success' => 'Visit count updated', 'visit_count' => $visit_count), 200);
        }
    }

    // Insert into  log table
    private function update_log($advertisement_id, $keyword, $event) {
        $this->load->model('api/log_model');

        $log = array(
            'advertisement_id' => $advertisement_id,
            'keyword' => $keyword,
            'ipaddress' => $_SERVER['REMOTE_ADDR'],
            'event' => $event
        );

        $this->log_model->insert($log);
    }

    // Match the keyword phrase from the result
    private function filter_adv($advs, $keywords) {
        foreach ($advs as $index => $row) {
            $keyword_array = explode(', ', $row['keyword']);

            if (in_array($keywords, $keyword_array)) {
                return $index;
            }
        }

        $this->response(array('error' => 'No Advertisement Found'), 200);
    }

}
