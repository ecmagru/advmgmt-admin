<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Signin extends Frontend_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('users_model');
    }

    public function index() {
        $this->load->library('form_validation');

        if ($this->input->post('submit') && $this->input->post('submit') == 'submit') {
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'required|trim');

            $this->form_validation->set_message('required', '%s is required');
            $this->form_validation->set_error_delimiters('<p class="help-block">', '</p>');

            if ($this->form_validation->run() == TRUE) {
                $conditions = array(
                    'email' => $this->input->post('email'),
                    'password' => md5($this->input->post('password')),
                    'status' => 1
                );

                $result = $this->users_model->as_array()->get_by($conditions);

                if (empty($result)) {
                    $this->session->set_flashdata('flashdata', array('type' => 'error', 'text' => config_item('error_invalid_login')));
                    redirect(base_url('signin'));
                    exit();
                }

                // Success
                $session = array(
                    'user_id' => $result['user_id'],
                    'user_role' => $result['type'],
                    'is_logged_in' => TRUE
                );

                $this->session->set_userdata(array('user_data' => $session));

                if ($result['type'] == 1) {
                    // User
                    redirect(base_url('user/advertisement'));
                    exit();
                } else {
                    // Admin
                    redirect(base_url('admin/dashboard'));
                    exit();
                }
            }
        }

        $this->template->build('frontend/signin', $this->data);
    }

}
