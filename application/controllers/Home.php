<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Home extends Frontend_Controller {

	function __construct() {
		parent::__construct();

	}

	public function index() {
		$this->template->build('frontend/signup', $this->data);
	}

	public function banner($keyword = NULL) {
		$this->load->model('api/advertisement_model');
		$keyword = urldecode($keyword);

		if(is_null($keyword)) {
			$this->template->build('frontend/signup', $this->data);
		} else {
			$result = $this->advertisement_model->get_advertisement($keyword);

			if (sizeof($result) < 1) {
				echo "Incorrect keyword.";
			} else {
				if ($this->filter_adv($result, $keyword) > -1) {
					$result = $result[$this->filter_adv($result, $keyword)];
					$this->update_log($result['advertisement_id'], $keyword, "visit");
					$click_count = $result['click_count'] + 1;
					$this->advertisement_model->update_by(array('advertisement_id' => $result['advertisement_id']), array('click_count' => $click_count));
					$result['image_url'] = base_url('uploads/' . $result['image_url']);
					$this->data['keyword'] = $keyword;
					$this->data['advertisement'] = $result;
					// header("Referer: https://keywordadmin.net");
					// header("location: " . $result['url']);
					$this->template->build('frontend/banner', $this->data);
				} else {
					echo "Incorrect keyword";
				}
			}
		}
	}

	public function click($keyword = NULL) {

		$this->load->model('api/advertisement_model');
		$keyword = urldecode($keyword);

		if(is_null($keyword)) {
			$this->template->build('frontend/signup', $this->data);
		} else {
			$result = $this->advertisement_model->get_advertisement($keyword);

			if (sizeof($result) < 1) {
				echo "Incorrect keyword.";
			} else {
				if ($this->filter_adv($result, $keyword) > -1) {
					$result = $result[$this->filter_adv($result, $keyword)];
					$this->update_log($result['advertisement_id'], $keyword, "tracked visits");

					$click_count = $result['click_count'] + 1;
					$this->advertisement_model->update_by(array('advertisement_id' => $result['advertisement_id']), array('click_count' => $click_count));
					
					$this->template->build('frontend/banner', $this->data);
					header("Referer: https://keywordadmin.net");
					header("location: " . $result['url']);
				} else {
					echo "Incorrect keyword";
				}
			}
		}
	}

	// Insert into  log table
	private function update_log($advertisement_id, $keyword, $event) {
		$this->load->model('api/log_model');

		$log = array(
			'advertisement_id' => $advertisement_id,
			'keyword' => $keyword,
			'ipaddress' => $_SERVER['REMOTE_ADDR'],
			'event' => $event
		);

		$this->log_model->insert($log);
	}

	// Match the keyword phrase from the result
	private function filter_adv($advs, $keywords) {
		foreach ($advs as $index => $row) {
			$keyword_array = explode(', ', $row['keyword']);

			if (in_array($keywords, $keyword_array)) {
				return $index;
			}
		}

		return -1;
	}
}
