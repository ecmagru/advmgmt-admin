<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Advertisement extends User_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('advertisement_model');
        $this->load->model('advertisement_keyword_model');
    }

    public function index() {
        $this->data['advertisement_list'] = $this->advertisement_model->get_user_advertisements(array('advertisement_user.user_id' => $this->get_user_id()));

        $this->template->build('user/advertisement/index', $this->data);
    }

    public function log($advertisement_id = NULL) {
        if (is_null($advertisement_id)) {
            show_404();
        }

        $result = $this->advertisement_model->as_array()->get_by(array('unique_id' => $advertisement_id));
        if (empty($result)) {
            show_404();
        }

        $this->load->model('log_model');

        // Search
        $date_from = NULL;
        $date_to = NULL;

        if ($this->input->get('submit') && $this->input->get('submit') == 'SEARCH') {
            if ($this->input->get('date_from')) {
                $date_from = date('Y-m-d', strtotime($this->input->get('date_from')));
            }

            if ($this->input->get('date_to')) {
                $date_to = date('Y-m-d', strtotime($this->input->get('date_to')));
            }
        }

        $this->template->set_js(array('assets/lib/bootstrap-datepicker/js/bootstrap-datepicker.min.js'));
        $this->template->set_css(array('assets/lib/bootstrap-datepicker/css/bootstrap-datepicker.min.css'));

        $this->data['logs'] = $this->log_model->get_logs($date_from, $date_to, $advertisement_id);
        $this->data['date_from'] = $this->input->get('date_from');
        $this->data['date_to'] = $this->input->get('date_to');
        $this->data['advertisement_id'] = $advertisement_id;

        $this->template->build('user/logs/index', $this->data);
    }

}
