<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Logout extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $userdata = array('user_id' => '', 'user_role' => '', 'is_logged_in' => '');
        $this->session->unset_userdata($userdata);
        $this->session->sess_destroy();
        redirect(base_url('signin'), 'refresh');
    }

}
