<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users extends Admin_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('users_model');
        $this->load->model('advertisement_model');
        $this->load->model('advertisement_keyword_model');
        $this->load->model('advertisement_user_model');
    }

    public function index() {
        $this->data['users'] = $this->users_model->as_array()->get_all();

        $this->template->build('admin/users/index', $this->data);
    }

    public function create() {
        $this->load->library('form_validation');

        if ($this->input->post('submit') && $this->input->post('submit') == 'submit') {
            $this->form_validation->set_rules('name', 'Title', 'required|trim|max_length[100]');
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|max_length[100]');
            $this->form_validation->set_rules('password', 'Password', 'required|trim|max_length[20]|min_length[6]');
            $this->form_validation->set_rules('user_role', 'Role', 'required|trim');

            $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');
            $this->form_validation->set_message('required', '%s is required');

            if ($this->form_validation->run() == TRUE) {
                $formData = array(
                    'name' => $this->input->post('name'),
                    'email' => $this->input->post('email'),
                    'password' => md5($this->input->post('password')),
                    'type' => $this->input->post('user_role'),
                    'date_created' => date('Y-m-d H:i:s')
                );

                if ($user_id = $this->users_model->insert($formData)) {
                    $this->session->set_flashdata('flashdata', array('type' => 'success', 'text' => config_item('success_user_created')));
                }

                // Assign the adv to user
                $advertisements = $this->input->post('advertisements');
                if (!empty($advertisements)) {
                    foreach ($advertisements as $adv) {
                        $this->advertisement_user_model->insert(array('advertisement_id' => $adv, 'user_id' => $user_id));
                    }
                }

                redirect(base_url('admin/users'));
                exit();
            }
        }

        $this->data['advertisements'] = $this->advertisement_model->get_advertisements_not_assigned();

        $this->template->build('admin/users/create', $this->data);
    }

    public function edit($user_id = NULL) {
        if (is_null($user_id) || !is_numeric($user_id)) {
            show_404();
        }

        $result = $this->users_model->as_array()->get_by(array('user_id' => $user_id));
        if (empty($result)) {
            show_404();
        }

        $this->load->library('form_validation');

        if ($this->input->post('submit') && $this->input->post('submit') == 'submit') {
            $this->form_validation->set_rules('name', 'Title', 'required|trim|max_length[100]');
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|max_length[100]');
            $this->form_validation->set_rules('user_role', 'Role', 'required|trim');

            $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');
            $this->form_validation->set_message('required', '%s is required');

            if ($this->form_validation->run() == TRUE) {
                $formData = array(
                    'name' => $this->input->post('name'),
                    'email' => $this->input->post('email'),
                    'type' => $this->input->post('user_role')
                );

                if ($this->users_model->update_by(array('user_id' => $user_id), $formData)) {
                    $this->session->set_flashdata('flashdata', array('type' => 'success', 'text' => config_item('success_user_updated')));
                }

                // Assign the adv to user
                $this->advertisement_user_model->delete_by(array('user_id' => $user_id));
                $advertisements = $this->input->post('advertisements');
                if (!empty($advertisements)) {
                    foreach ($advertisements as $adv) {
                        $this->advertisement_user_model->insert(array('advertisement_id' => $adv, 'user_id' => $user_id));
                    }
                }

                redirect(base_url('admin/users'));
                exit();
            }
        }

        // Get Available Advs
        $this->data['advertisements'] = $this->advertisement_model->get_advertisements_not_assigned();

        $this->data['user'] = $result;

        $this->template->build('admin/users/edit', $this->data);
    }

    public function delete($user_id = NULL) {
        if (is_null($user_id) || !is_numeric($user_id)) {
            show_404();
        }

        // Deny super admin deletion
        if ($user_id == 2) {
            $this->session->set_flashdata('flashdata', array('type' => 'error', 'text' => config_item('error_deny_user_deletion')));
            redirect(base_url('admin/users'), 'refresh');
            exit();
        }

        $result = $this->users_model->as_array()->get_by(array('user_id' => $user_id));
        if (empty($result)) {
            show_404();
        }

        if ($this->users_model->delete($user_id)) {
            $this->session->set_flashdata('flashdata', array('type' => 'success', 'text' => config_item('success_user_deleted')));
            redirect(base_url('admin/users'), 'refresh');
            exit();
        }
    }

}
