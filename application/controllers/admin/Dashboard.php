<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends Admin_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->template->build('admin/dashboard/dashboard', $this->data);
    }

}
