<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Logs extends Admin_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('log_model');
    }

    public function index() {
        // Search
        $date_from = NULL;
        $date_to = NULL;

        if ($this->input->get('submit') && $this->input->get('submit') == 'SEARCH') {
            if ($this->input->get('date_from')) {
                $date_from = date('Y-m-d H:i:s', strtotime($this->input->get('date_from')));
            }

            if ($this->input->get('date_to')) {
                $date_to = date('Y-m-d H:i:s', strtotime($this->input->get('date_to')));
            }
        }

        $this->template->set_js(array('assets/lib/bootstrap-datepicker/js/bootstrap-datepicker.min.js'));
        $this->template->set_css(array('assets/lib/bootstrap-datepicker/css/bootstrap-datepicker.min.css'));

        $this->data['logs'] = $this->log_model->get_all_logs($date_from, $date_to);
        $this->data['date_from'] = $this->input->get('date_from');
        $this->data['date_to'] = $this->input->get('date_to');

        $this->template->build('admin/logs/index', $this->data);
    }

}
