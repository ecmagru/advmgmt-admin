<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Keywords extends Admin_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('advertisement_model');
    }

    public function index() {
        $this->data['keywords'] = $this->advertisement_model->get_all_advertisement(NULL, NULL, array());

        $this->template->build('admin/advertisement/keywords', $this->data);
    }

}
