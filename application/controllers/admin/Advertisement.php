<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Advertisement extends Admin_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('advertisement_model');
        $this->load->model('advertisement_keyword_model');
    }

    public function index() {
        // Search
        $date_from = NULL;
        $date_to = NULL;

        if ($this->input->get('submit') && $this->input->get('submit') == 'SEARCH') {
            if ($this->input->get('date_from')) {
                $date_from = date('Y-m-d', strtotime($this->input->get('date_from')));
            }

            if ($this->input->get('date_to')) {
                $date_to = date('Y-m-d', strtotime($this->input->get('date_to')));
            }
        }

        $advertisement_list = $this->advertisement_model->get_all_advertisement($date_from, $date_to);
        $this->data['date_from'] = $this->input->get('date_from');
        $this->data['date_to'] = $this->input->get('date_to');
        $adv_array = array();

        if (!empty($advertisement_list)) {
            $advertisement_id = NULL;

            foreach ($advertisement_list as $row) {
                if ($advertisement_id != $row['advertisement_id']) {
                    $advertisement_id = $row['advertisement_id'];

                    $adv_array[$advertisement_id]['advertisement_id'] = $row['advertisement_id'];
                    $adv_array[$advertisement_id]['title'] = $row['title'];
                    $adv_array[$advertisement_id]['traffic_string'] = $row['traffic_string'];
                    $adv_array[$advertisement_id]['url'] = $row['url'];
                    $adv_array[$advertisement_id]['visit_count'] = $row['visit_count'];
                    $adv_array[$advertisement_id]['click_count'] = $row['click_count'];
                    $adv_array[$advertisement_id]['status'] = $row['status'];
                    $adv_array[$advertisement_id]['unique_id'] = $row['unique_id'];
                    $adv_array[$advertisement_id]['date_created'] = $row['date_created'];
                    $adv_array[$advertisement_id]['user_assigned'] = $row['name'];
                    $adv_array[$advertisement_id]['keywords'] = array();
                }

                array_push($adv_array[$advertisement_id]['keywords'], $row['keyword']);
            }
        }

        $this->data['advertisement_list'] = $adv_array;

        $this->template->set_js(array('assets/lib/bootstrap-datepicker/js/bootstrap-datepicker.min.js'));
        $this->template->set_css(array('assets/lib/bootstrap-datepicker/css/bootstrap-datepicker.min.css'));

        $this->template->build('admin/advertisement/index', $this->data);
    }

    public function log($advertisement_id = NULL) {
        if (is_null($advertisement_id)) {
            show_404();
        }

        $result = $this->advertisement_model->as_array()->get_by(array('unique_id' => $advertisement_id));
        if (empty($result)) {
            show_404();
        }

        $this->load->model('log_model');

        // Search
        $date_from = NULL;
        $date_to = NULL;

        if ($this->input->get('submit') && $this->input->get('submit') == 'SEARCH') {
            if ($this->input->get('date_from')) {
                $date_from = date('Y-m-d', strtotime($this->input->get('date_from')));
            }

            if ($this->input->get('date_to')) {
                $date_to = date('Y-m-d', strtotime($this->input->get('date_to')));
            }
        }

        $this->template->set_js(array('assets/lib/bootstrap-datepicker/js/bootstrap-datepicker.min.js'));
        $this->template->set_css(array('assets/lib/bootstrap-datepicker/css/bootstrap-datepicker.min.css'));

        $this->data['logs'] = $this->log_model->get_logs($date_from, $date_to, $advertisement_id);
        $this->data['date_from'] = $this->input->get('date_from');
        $this->data['date_to'] = $this->input->get('date_to');
        $this->data['advertisement_id'] = $advertisement_id;

        $this->template->build('admin/logs/index', $this->data);
    }

    public function create() {
        $this->load->library('form_validation');

        if ($this->input->post('submit') && $this->input->post('submit') == 'submit') {
            $this->form_validation->set_rules('title', 'Title', 'required|trim|max_length[300]');
            $this->form_validation->set_rules('url', 'URL', 'required|trim|max_length[300]');
            $this->form_validation->set_rules('keyword', 'Keyword', 'required|trim|max_length[1000]|callback_check_keywords');
            $this->form_validation->set_rules('file_name', 'File', 'required|trim|max_length[300]');
            $this->form_validation->set_rules('file_url', 'File', 'required|trim');
            $this->form_validation->set_rules('traffic_string', 'Traffic String', 'trim');

            $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');
            $this->form_validation->set_message('required', '%s is required');
            $this->form_validation->set_message('is_unique', 'Keyword should be unique');

            if ($this->form_validation->run() == TRUE) {
                $this->load->helper('string');

                $formData = array(
                    'user_id' => $this->get_user_id(),
                    'title' => $this->input->post('title'),
                    'url' => $this->input->post('url'),
                    'image_url' => $this->input->post('file_name'),
                    'unique_id' => random_string('alnum', 20),
                    'traffic_string' => $this->input->post('traffic_string'),
                    'date_created' => date('Y-m-d H:i:s')
                );

                $advertisement_id = $this->advertisement_model->insert($formData);
                unset($formData);

                // Save the keywords
                $keywords = explode(',', strtolower($this->input->post('keyword')));
                foreach ($keywords as $keyword) {
                    $this->advertisement_keyword_model->insert(array('advertisement_id' => $advertisement_id, 'keyword' => trim($keyword)));
                }

                if ($advertisement_id) {
                    $this->session->set_flashdata('flashdata', array('type' => 'success', 'text' => config_item('success_advertisement_created')));
                    redirect(base_url('admin/advertisement'));
                    exit();
                }
            }
        }

        $this->template->set_js(array('assets/lib/uploader/js/vendor/jquery.ui.widget.js', 'assets/lib/uploader/js/jquery.iframe-transport.js', 'assets/lib/uploader/js/jquery.fileupload.js'));
        $this->template->set_css(array('assets/lib/uploader/css/jquery.fileupload-ui.css', 'assets/lib/uploader/css/jquery.fileupload.css'));

        $this->template->build('admin/advertisement/create', $this->data);
    }

    public function delete($advertisement_id = NULL) {
        if (is_null($advertisement_id)) {
            show_404();
        }

        $result = $this->advertisement_model->as_array()->get_by(array('unique_id' => $advertisement_id));
        if (empty($result)) {
            show_404();
        }

        if ($this->advertisement_model->delete_by(array('unique_id' => $advertisement_id))) {
            $this->session->set_flashdata('flashdata', array('type' => 'success', 'text' => config_item('success_advertisement_deleted')));
            redirect(base_url('admin/advertisement'));
            exit();
        }
    }

    public function edit($advertisement_id = NULL) {
        if (is_null($advertisement_id)) {
            show_404();
        }

        $result = $this->advertisement_model->as_array()->get_by(array('unique_id' => $advertisement_id));
        if (empty($result)) {
            show_404();
        }

        $advertisement = $this->advertisement_model->get_advertisement(array('advertisement.advertisement_id' => $result['advertisement_id']));

        $this->load->library('form_validation');

        if ($this->input->post('submit') && $this->input->post('submit') == 'submit') {
            $this->form_validation->set_rules('title', 'Title', 'required|trim|max_length[300]');
            $this->form_validation->set_rules('url', 'URL', 'required|trim|max_length[300]');
            $this->form_validation->set_rules('file_name', 'File', 'required|trim|max_length[300]');
            $this->form_validation->set_rules('file_url', 'File', 'required|trim');
            $this->form_validation->set_rules('traffic_string', 'Traffic String', 'trim');
            $this->form_validation->set_rules('keyword', 'Keyword', 'required|trim|max_length[1000]|callback_check_keywords');

            $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');
            $this->form_validation->set_message('required', '%s is required');

            // Keywords validation
            $keywords = array_map('trim', explode(', ', $this->input->post('keyword')));
            $keywords_old = $advertisement['keywords'];

            $matches = array_diff($keywords, $keywords_old);
            if (!empty($matches)) {
                $this->form_validation->set_rules('keyword', 'Keyword', 'required|trim|max_length[1000]|callback_check_keywords');
            }

            if ($this->form_validation->run() == TRUE) {
                $this->load->helper('string');

                $formData = array(
                    'title' => $this->input->post('title'),
                    'url' => $this->input->post('url'),
                    'image_url' => $this->input->post('file_name'),
                    'traffic_string' => $this->input->post('traffic_string')
                );

                $this->advertisement_model->update_by(array('unique_id' => $advertisement_id), $formData);
                unset($formData);

                // Save the keywords
                $this->advertisement_keyword_model->delete_by(array('advertisement_id' => $advertisement['advertisement_id']));
                $keywords = explode(',', $this->input->post('keyword'));
                foreach ($keywords as $keyword) {
                    $this->advertisement_keyword_model->insert(array('advertisement_id' => $advertisement['advertisement_id'], 'keyword' => strtolower(trim($keyword))));
                }

                $this->session->set_flashdata('flashdata', array('type' => 'success', 'text' => config_item('success_advertisement_updated')));
                redirect(base_url('admin/advertisement'));
                exit();
            }
        }

        $this->data['advertisement'] = $advertisement;
        $this->data['file_name'] = isset($_POST['file_name']) ? $_POST['file_name'] : $result['image_url'];
        $this->data['file_url'] = isset($_POST['file_name']) ? $_POST['file_url'] : base_url('uploads/' . $result['image_url']);

        $this->template->set_js(array('assets/lib/uploader/js/vendor/jquery.ui.widget.js', 'assets/lib/uploader/js/jquery.iframe-transport.js', 'assets/lib/uploader/js/jquery.fileupload.js'));
        $this->template->set_css(array('assets/lib/uploader/css/jquery.fileupload-ui.css', 'assets/lib/uploader/css/jquery.fileupload.css'));

        $this->template->build('admin/advertisement/edit', $this->data);
    }

    public function users($advertisement_id = NULL) {
        if (is_null($advertisement_id)) {
            show_404();
        }

        $result = $this->advertisement_model->as_array()->get_by(array('unique_id' => $advertisement_id));
        if (empty($result)) {
            show_404();
        }

        $this->load->model('advertisement_users_model');

        $this->data['users'] = $this->advertisement_users_model->get_adv_users(array('advertisement_id' => $result['advertisement_id']));

        $this->template->build('admin/advertisement/users', $this->data);
    }

    public function check_keywords() {
        $advertisement_id = ($this->input->post('advertisement_id')) ? $this->input->post('advertisement_id') : NULL;

        $keyword = array_map('trim', explode(',', $this->input->post('keyword')));
        $all_keywords = $this->advertisement_keyword_model->as_array()->get_all();

        if (!empty($all_keywords)) {
            foreach ($all_keywords as $row) {
                if ($row['advertisement_id'] != $advertisement_id) {
                    $keywords_array[] = $row['keyword'];
                }
            }
        }

        $matches = array_intersect($keyword, $keywords_array);

        if (!empty($matches)) {
            $this->form_validation->set_message('check_keywords', 'Keyword should be unique');
            return FALSE;
        }

        return TRUE;
    }

}
