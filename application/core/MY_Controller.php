<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    protected $data = array();

    function __construct() {
        parent::__construct();

        // Site name
        $this->data['site_name'] = config_item('site_name');
        //   $this->output->enable_profiler(TRUE);
    }

}
