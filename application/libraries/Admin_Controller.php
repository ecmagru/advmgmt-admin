<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin_Controller extends Frontend_Controller {

    function __construct() {
        parent::__construct();

        // If not logged in redirect to login page
        if (!$this->is_logged_in()) {
            redirect(base_url('logout'));
            exit();
        }

        if ($this->get_user_role() != 2) {
            redirect(base_url('logout'));
            exit();
        }

        $header_menu[] = anchor(base_url('admin/users'), 'Users');
        $header_menu[] = anchor(base_url('admin/advertisement'), 'Advertisement');
        $header_menu[] = anchor(base_url('admin/keywords'), 'Keywords');

        $this->data['header_menu'] = array_merge($header_menu, $this->data['header_menu']);
        //$this->output->enable_profiler(TRUE);
    }

}
