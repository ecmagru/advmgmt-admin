<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Frontend_Controller extends MY_Controller {

    protected $userdata;

    function __construct() {
        parent::__construct();

        // Get the userdata, if the user logged in
        $userdata = $this->session->userdata('user_data');
        if (!empty($userdata)) {
            $this->userdata = $userdata;
        }

        $this->template->set_layout('frontend');
        $this->template->set_partial('header', 'frontend/components/header');

        // Reading flashdata
        $this->data['flashdata'] = $this->load->view('frontend/components/flashdata', '', TRUE);

        // Header menu
        if (!$this->is_logged_in()) {
            $header_menu[] = anchor(base_url('signin'), 'Sign In');
            $header_menu[] = anchor(base_url('signup'), 'Sign Up');
        }

        if ($this->is_logged_in()) {
            $header_menu[] = anchor(base_url('logout'), 'Logout');
        }

        $this->data['header_menu'] = $header_menu;
    }

    protected function get_user_id() {
        return $this->userdata['user_id'];
    }

    protected function get_user_role() {
        return $this->userdata['user_role'];
    }

    protected function is_logged_in() {
        return ($this->userdata['is_logged_in']) ? TRUE : FALSE;
    }

}
