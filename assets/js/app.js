$('document').ready(function () {
    // Confirm delete
    $('.row-delete').on('click', function () {
        if (confirm("Do you want to proceed ?") === true) {
            return true;
        } else {
            return false;
        }
        event.preventDefault();
    });

    // Upload
    if ($('#file_upload').length > 0) {
        $('#file_upload').fileupload({
            dataType: 'json',
            done: function (e, data) {
                var result = data.result.files;
                $('#file_name').val(result.name);
                $('#file_url').val(result.url);
                $('#image_placeholder').attr('src', result.url);
                $('#progress > p').text('Uploading...').remove();
                $('#progress .bar').hide();
            },
            add: function (e, data) {
                $('#progress .bar').show();
                data.context = $('<p/>').text('Uploading...').appendTo('#progress');
                data.submit();

            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress .bar').css('width', progress + '%');
            }
        });
    }

    // Datepicker
    if ($('.datepicker').length > 0) {
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });
    }
})